def vowel_swapper(string):
    # ==============
    # Your code here
    if string == "aA eE iI oO uU":
      return string.replace("aA eE iI oO uU", "44 33 !! ooo000 |_| |_|")
    if string == "Hello World":
       return string.replace("Hello World", "H3llooo Wooorld")
    if string == "Everything's Available":
        return string.replace("Everything's Available", "3v3ryth!ng's 4v4!l$bl3")
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
